var Task = function (name, description, code, price, start_Date, start_Hour, start_Min, end_Date, end_Hour, end_Min, listMarcator, listGeographic){
this.taskName = name;
this.taskDescription = description;
this.taskCode = code;
this.taskPrice = price;

this.startDate = start_Date;
this.startHour = start_Hour;
this.startMin = start_Min;
this.endDate = end_Date;
this.endHour = end_Hour;
this.endMin = end_Min;

this.AreaList_Mercator = listMarcator;
this.AreaList_Geographic = listGeographic;
}
$.extend(Task.prototype, {
getName: function() {return this.taskName;},
getDescription: function() {return this.taskDescription;},
getCode: function() {return this.taskCode;},
getPrice: function() {return this.taskPrice;},
getStartDate: function() {return this.startDate;},
getStartHour: function() {return this.startHour;},
getStartMin: function() {return this.startMin;},
getEndDate: function() {return this.endDate;},
getEndHour: function() {return this.endHour;},
getEndMin: function() {return this.endMin;},
getListMercator: function() {return this.AreaList_Mercator;},
getListGeographic: function() {return this.AreaList_Geographic;}

});
	
	
	
